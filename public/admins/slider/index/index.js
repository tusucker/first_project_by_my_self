const GET_METHOD = 'GET';
const POST_METHOD = 'POST';
const DELETE_METHOD = 'DELETE';

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#slider_image')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
            $('#image_path')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {

    let url_update;

    getListWithFileInput($('#table_slider'));

    // show modal create slider
    $(document).on('click', '.create-modal', function () {
        $('#form_create_slider')[0].reset();
        $('#modal_create_slider').modal('show');
    });

    // save slider into database with jquery ajax
    $(document).on('click', '#btn_create_slider', function () {
        let url = $(this).data('url');
        let data = new FormData($('#form_create_slider')[0]);
        callApiWithFileInput(POST_METHOD, data, url)
            .then(() => {
                getListWithFileInput($('#table_slider'));
                $('#modal_create_slider').modal('hide');
            })
    });

    //remove slider
    $(document).on('click', '#buton_remove', function () {
        let url = $(this).data('url');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                callApiWithFileInput(GET_METHOD, null, url)
                    .then(() => {
                        getListWithFileInput($('#table_slider'));
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    })
            }
        })
    });

    // show modal update slider
    $(document).on('click', '.show-modal', function () {
        url_update = $(this).data('url_update');
        let name = $(this).data('name');
        let description = $(this).data('description');
        let image_path = $(this).data('image_path');
        $('#name').val(name);
        $('#description').val(description);
        $('#image_path').attr('src', image_path);
        $('#modal_update_slider').modal('show');
    });

    // update slider into database with jquery ajax
    $(document).on('click', '#btn_update_slider', function () {
        let data = new FormData($('#form_update_slider')[0]);
        callApiWithFileInput(POST_METHOD, data, url_update)
            .then(() => {
                getListWithFileInput($('#table_slider'));
                $('#modal_update_slider').modal('hide');
            })
    });
});
