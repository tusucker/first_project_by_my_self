const GET_METHOD = 'GET';
const POST_METHOD = 'POST';

$(document).ready(function () {

    getList($('#table-role'));

    // handle click checkbox
    $(document).on('click', '.checkbox_wrapper', function () {
        $(this).parents('.card').find('.checkbox_child').prop('checked', $(this).prop('checked'));
    });

    // hande click check all
    $(document).on('click', '.checkAll', function () {
        $(this).parents().find('.checkbox_child').prop('checked', $(this).prop('checked'));
    });

    // open modal create role
    $(document).on('click', '.create-modal', function () {
        $('#form_create_role')[0].reset();
        $('#modal_create_role').modal('show');
    });

    // insert role into database
    $(document).on('click', '#btn_create_role', function () {
        let url = $(this).data('url');
        let data = $('#form_create_role').serialize();
        callApi(POST_METHOD, data, url)
            .then(() => {
                getList($('#table-role'));
                $('#modal_create_role').modal('hide');
            })
    });

    // getdata and open modal update role
    $(document).on('click', '.show-modal', function () {
        // get data and fill into form
        let url = $(this).data('url');
        callApi(GET_METHOD, {}, url)
            .then((rs) => {
                $('#get_data_and_fill').replaceWith(rs);
            })
            .catch((error) => {
                console.log(error);
            })
        $('#modal_update_role').modal('show');
    });

    // update role into database
    $(document).on('click', '#btn_update_role', function () {
        let url = $(this).data('url');
        let data = $('#form_update_role').serialize();
        callApi(POST_METHOD, data, url)
            .then(() => {
                location.reload();
                //getList($('#table-role'));
                //$('#modal_update_role').modal('hide');
            })
            .catch((error) => {
                console.log(error);
            })
    });

    //delete role
    $(document).on('click', '.delete-modal', function () {
        let url = $(this).data('url');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                callApi(GET_METHOD, {}, url)
                    .then(() => {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                        getList($('#table-role'));
                    })
            }
        })
    });
});
