const GET_METHOD = 'GET';
const POST_METHOD = 'POST';


// function delete product
function deleteProduct() {
    let url = $(this).data('action');
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            callApiWithFileInput(GET_METHOD, {}, url)
                .then(() => {
                    getListWithFileInput($('#table_product'));
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                })
        }
    })
}

// function set url image
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#product_image')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
            $('#feature_image')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

// number_format
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function fetchData(url, page) {
    url = url + "?page=" + page;
    callApi(GET_METHOD, {}, url).then((rs) => {
        $('#table_product').replaceWith(rs);
    })
}

$(function () {
    let url_load_table = $('#table_product').data('url');
    let url_update;

    // load table product
    getListWithFileInput($('#table_product'));

    //click to change page
    $(document).on('click', '.pagination a', function (event) {
        event.preventDefault();
        let page = $(this).attr('href').split('page=')[1];
        fetchData(url_load_table,page);
    });

    // event onchange category
    $(document).on('change', '#search_by_category', function () {
        let category_id = $(this).val();
        callApi(null, null, url_load_table + '?category_id=' + category_id).then((rs) => {
            $('#table_product').replaceWith(rs);
        });
    });

    // event keyup search
    $(document).on('keyup', '#search_product', function () {
        let data = {
            category_id: $('#search_by_category').val(),
            data_input_search: $('#search_product').val()
        }
        callApi(null, null, url_load_table + '?category_id=' + data.category_id +
            '&data_input_search=' + data.data_input_search
        ).then((rs) => {
            $('#table_product').replaceWith(rs);
        });
    });

    //click delete product
    $(document).on('click', '#btn_delete_product', deleteProduct);

    //open modal
    $(document).on('click', '#btn_open_modal_create', function () {
        $('#form_create_product')[0].reset();
        $('#modal_create').modal();
    });

    //create new product
    $(document).on('click', '#btn_create_product', function () {
        let url = $(this).data('url');
        // save value textarea into data
        tinyMCE.get("getData").save();
        let data = new FormData($('#form_create_product')[0]);
        callApiWithFileInput(POST_METHOD, data, url)
            .then(() => {
                getListWithFileInput($('#table_product'));
                $('#modal_create').modal('hide');
            })
    });

    //open and fill data product
    $(document).on('click', '#btn_update_product', function () {
        let url = $(this).data('action');
        url_update = $(this).data('url');
        callApiWithFileInput(GET_METHOD, {}, url)
            .then((res) => {
                let productImage = '';
                let tags = '';
                tinyMCE.get('product_content').setContent(res.product.content);
                res.list_product_image.forEach(function (value) {
                    productImage += '<div class="col-md-3 m-3"><img src="' + value.image_path + '" alt="" style="width: 150px;height: 100px;object-fit: cover"></div>';
                });
                res.list_tag.forEach(function (value) {
                    tags += '<option selected value="' + value.name + '">' + value.name + '</option>';
                });
                $('#product_name_update').val(res.product.name);
                $('#product_price_update').val(res.product.price);
                $('#feature_image').attr('src', res.product.avatar);
                $('#row_contain_image_detail').html(productImage);
                $('#category_id').html(res.htmlOption);
                $('#list_tag').html(tags);
                $('#model_update_product').modal('show');
            })
    });

    // update product
    $(document).on('click', '#btn_update_submit', function () {
        tinyMCE.get("product_content").save();
        let data = new FormData($('#form_update_product')[0]);
        callApiWithFileInput(POST_METHOD, data, url_update)
            .then(() => {
                getListWithFileInput($('#table_product'));
                $('#model_update_product').modal('hide');
            })
    });
})
