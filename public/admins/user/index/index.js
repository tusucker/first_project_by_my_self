const GET_METHOD = 'GET';
const POST_METHOD = 'POST';

$(document).ready(function () {

    let url_update;

    getList($('#table-user'));

    //open modal create user
    $(document).on('click', '.create-modal', function () {
        $('#form_create_user')[0].reset();
        $('#modal_create_user').modal('show');
    });

    // insert user into database
    $(document).on('click', '#btn_insert_user', function () {
        let url = $(this).data('url');
        let data = $('#form_create_user').serialize();
        callApi(POST_METHOD, data, url)
            .then(() => {
                getList($('#table-user'));
                $('#modal_create_user').modal('hide');
            })
    });

    // open modal update user
    $(document).on('click', '.show-modal', function () {
        //get data to fill input form
        let url = $(this).data('url');
        url_update = $(this).data('action');
        callApi(GET_METHOD, {}, url)
            .then((rs) => {
                $('#name_update').val(rs.user.name);
                $('#email_update').val(rs.user.email);
                $('#password_update').val(rs.user.password);
                $('#user_role_update').html(rs.roleSelected);
                $('#modal_update_user').modal('show');
            })
        $('#modal_update_user').modal('show');
    });

    //update user
    $(document).on('click', '#btn_update_user', function () {
        let data = $('#form_update_user').serialize();
        callApi(POST_METHOD, data, url_update)
            .then(() => {
                getList($('#table-user'));
                $('#modal_update_user').modal('hide');
            })
    });

    //delete user
    $(document).on('click', '.delete-modal', function () {
        let url = $(this).data('url');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                callApi(GET_METHOD, {}, url)
                    .then(() => {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                        getList($('#table-user'));
                    })
            }
        })
    });
});
