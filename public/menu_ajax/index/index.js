const GET_METHOD = 'GET';
const POST_METHOD = 'POST';
const PUT_METHOD = 'PUT';
const DELETE_METHOD = 'DELETE';

function fetchData(page) {
    let url = "/menus/table?page=" + page;
    callApi(GET_METHOD, {}, url).then((data) => {
        $('#table_menu').html(data);
    })
}

$(document).ready(function () {
    getList($('#table_menu'));
    let url_update;

    $(document).on('keyup', '#menu_search', function () {
        let data = $(this).val();
        let url = $(this).data('url');
        callApi(null, null, url + '?search=' + data).then((rs) => {
            $('#table_menu').replaceWith(rs);
        })
    });

    $(document).on('click', '.pagination a', function (event) {
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        fetchData(page);
    });

    // click add to create new category
    $(".create-modal").click(function () {
        //console.log('click me');
        $("#menu_name").val('');
        $("#menu_parent").val(0);
        $("#menu_create_modal").modal('show');
    });
    // save category
    $(document).on('click', '#btn_save_menu', function () {
        let url = $(this).data('url');
        let data = $('#form_add').serialize();
        callApi(POST_METHOD, data, url).then((rs) => {
            $("#menu_parent").empty().append('<option value="0">Đây là menu cha</option>' + rs.optionSelect);
            $("#menu_parent_update").empty().append('<option value="0">Đây là menu cha</option>' + rs.optionSelect);
            getList($('#table_menu'));
            $("#menu_create_modal").modal('hide');
        })

    });

    // show dialog delete category
    $(document).on('click', '.delete-modal', function () {
        let url = $(this).data('url');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                callApi(DELETE_METHOD, {}, url).then(() => {
                    getList($('#table_menu'));
                })
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success',
                )
            }
        })
    });

    // click Edit to update category
    $(document).on('click', '.show-modal', function () {
        url_update = $(this).data("url");
        let menu_name_update = $(this).data('name');
        let menu_parent_update = $(this).data('parent_id');
        $("#menu_name_update").val(menu_name_update);
        $("#menu_parent_update").val(menu_parent_update);
        $("#menu_update_modal").modal('show');
    });
    // save value updated by ajax
    $(document).on('click', '#btn_update_menu', function () {
        let data = $('#form_update_menu').serialize();
        callApi(PUT_METHOD, data, url_update).then((rs) => {
            getList($('#table_menu'));
            $("#menu_parent").empty().append('<option value="0">Đây là menu cha</option>' + rs.optionSelect);
            $("#menu_parent_update").empty().append('<option value="0">Đây là menu cha</option>' + rs.optionSelect);
            $("#menu_update_modal").modal('hide');
        })
    });
})
