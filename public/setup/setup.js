//set up ajax


$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})

function callApi(method, data, url) {
    return $.ajax({
        method: method,
        data: data,
        url: url
    });
}

function callApiWithFileInput(method, data, url) {
    return $.ajax({
        method: method,
        data: data,
        url: url,
        processData: false,
        contentType: false,
    });
}

function getList(selector) {
    let url = selector.data('url');
    callApi(GET_METHOD, {}, url)
        .then((rs) => {
            selector.replaceWith(rs);
        })
        .catch((error) => {
            alert(error);
        })
}

function getListWithFileInput(selector) {
    let url = selector.data('url');
    callApiWithFileInput(GET_METHOD, {}, url).then((rs) => {
        selector.replaceWith(rs);
    })
}


