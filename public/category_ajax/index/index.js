const GET_METHOD = 'GET';
const POST_METHOD = 'POST';
const PUT_METHOD = 'PUT';
const DELETE_METHOD = 'DELETE';

$(document).ready(function () {

    let url_update;

    getList($('#table_category'));

    // click add to create new category
    $(document).on('click', '.create-modal', function () {
        //console.log('click me');
        $("#category_name").val('');
        $("#category_parent").val(0);
        $("#create").modal();
    });

    // save category
    $(document).on('click', '#save_category', function () {
        let url = $(this).data('action');
        let data = $('#form_add').serialize();
        callApi(POST_METHOD, data, url)
            .then((res) => {
                $("#category_parent").empty().append('<option value="0">Đây là danh mục cha</option>' + res.htmlOption);
                $("#category_parent_update").empty().append('<option value="0">Đây là danh mục cha</option>' + res.htmlOption);
                $('#create').modal('hide');
                getList($('#table_category'));
            })
            .catch((res) => {
                $('#name_error').css('display', 'block');
                $('#name_error').html(res.responseJSON.errors.name);
            })
    });

    //delete category
    $(document).on('click', '.delete-modal', function () {
        let url = $(this).data('url');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success',
                    callApi(DELETE_METHOD, {}, url)
                        .then(() => {
                            $('#modal-delete').modal('hide');
                            getList($('#table_category'));
                        })
                        .catch((error) => {
                            alert(error);
                        })
                )
            }
        })
    });

// click Edit to update category
    $(document).on('click', '.show-modal', function () {
        url_update = $(this).data('url');
        let category_name_update = $(this).data('name');
        let parent_id_update = $(this).data('parent_id');
        $("#category_name_update").val(category_name_update);
        $("#parent_id_update").val(parent_id_update);
        $("#update_modal").modal('show');
    });

// save value updated by ajax
    $(document).on('click', '#btn_update_category', function () {
        parent_name = $("#category_parent_update option:selected").text().split('-').join('');
        let data = $('#form_update_category').serialize();
        callApi(PUT_METHOD, data, url_update)
            .then((res) => {
                $("#category_parent").empty().append('<option value="0">Đây là danh mục cha</option>' + res.htmlOption);
                $("#category_parent_update").empty().append('<option value="0">Đây là danh mục cha</option>' + res.htmlOption);
                $('#update_modal').modal('hide');
                getList($('#table_category'));
            })
            .catch((error) => {
                alert(error);
            })
    });
});





