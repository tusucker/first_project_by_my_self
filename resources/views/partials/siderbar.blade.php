<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Pet Shop</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="adminlte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                @can('category-list')
                    <li class="nav-item">
                        <a href="{{route('categories.index')}}" class="nav-link">
                            {{--                        <i class="nav-icon fas fa-th"></i>--}}
                            <i class="nav-icon far fa-caret-square-down"></i>
                            <p>
                                Danh mục sản phẩm
                            </p>
                        </a>
                    </li>
                @endcan
                @can('menu-list')
                    <li class="nav-item">
                        <a href="{{route('menus.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-bars"></i>
                            <p>
                                Menu
                            </p>
                        </a>
                    </li>
                @endcan
                @can('product-list')
                    <li class="nav-item">
                        <a href="{{route('products.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-paw"></i>
                            <p>
                                Products
                            </p>
                        </a>
                    </li>
                @endcan
                @can('slider-list')
                    <li class="nav-item">
                        <a href="{{route('sliders.index')}}" class="nav-link">
                            <i class="nav-icon far fa-images"></i>
                            <p>
                                Slider
                            </p>
                        </a>
                    </li>
                @endcan
                @can('user-list')
                    <li class="nav-item">
                        <a href="{{route('users.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Danh sách nhân viên
                            </p>
                        </a>
                    </li>
                @endcan
                @can('role-list')
                    <li class="nav-item">
                        <a href="{{route('roles.index')}}" class="nav-link">
                            <i class="nav-icon fab fa-buffer"></i>
                            <p>
                                Danh sách Role
                            </p>
                        </a>
                    </li>
                @endcan
{{--                <li class="nav-item">--}}
{{--                    <a href="{{route('permissions.create')}}" class="nav-link">--}}
{{--                        <i class="nav-icon fab fa-buffer"></i>--}}
{{--                        <p>--}}
{{--                            Tạo dữ liệu bảng--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
