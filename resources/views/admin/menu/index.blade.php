@extends('layouts.admin')
@section('title')
    <title>Menu - index</title>
@endsection
@section('css')

@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('partials.content-header',['name'=>'Menu','key'=>'List'])
    <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row padding_left_65">
                    <div class="col-md-6">
                        @can('menu-add')
                            <a class="create-modal btn btn-success m-3 text-bold text-white">Add</a>
                        @endcan
                    </div>
                    <div class="col-md-6 ">
                        <input type="text" id="menu_search" class="m-3"
                               style="margin-left: 38% !important;"
                               data-url="{{route('menus.table')}}">
                    </div>
                    <div class="col-md-10" id="table_menu" data-url="{{route('menus.table')}}">
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('admin.menu.add')
    @include('admin.menu.edit')
@endsection

@section('js')
    <script src="setup/setup.js"></script>
    <script src="menu_ajax/index/index.js"></script>
@endsection

