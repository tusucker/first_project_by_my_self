<div class="col-md-10" id="table_menu" data-url="{{route('menus.table')}}">

    <table class="table" id="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tên Menu</th>
            <th scope="col">Tên Menu cha</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
        </thead>
        <tbody id="body_table">
        @if(isset($menus))
            @foreach($menus as $item)
                <tr data-id="{{$item->id}}" data-parent={{$item->parent_id}}>
                    <td scope="row">{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    @if($item->parent_id==0)
                        <td>Đây là Menu cha</td>
                    @else
                        <td data-parent_id="{{$item->parent_id}}">{{$item->menu['name']}}</td>
                    @endif
                    <td class="text-center w-25">
                        @can('menu-edit')
                            <a style="width: 23%" class="show-modal btn btn-info"
                               data-id="{{$item->id}}" data-name="{{$item->name}}" data-parent_id="{{$item->parent_id}}"
                               data-url="{{route('menus.update',['id'=>$item->id])}}"
                            >Edit</a>
                        @endcan
                        @can('menu-delete')
                            <a class="delete-modal btn btn-danger" id="buton_remove"
                               data-url="{{route('menus.destroy',['id'=>$item->id])}}"
                            >Delete</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    {!! $menus->links() !!}
</div>
