<!-- Modal -->
<div class="modal fade" id="menu_update_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" id="form_update_menu">
                <div class="modal-body">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label>Tên Menu</label>
                            <input type="text" class="form-control" placeholder="Nhập tên menu" id="menu_name_update" name="name">
                        </div>
                        <div class="form-group">
                            <label>Chọn menu cha</label>
                            <select class="form-control" id="menu_parent_update" name="parent_id">
                                <option value='0'>Đây là menu cha</option>
                                {!! $optionSelect !!}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn_update_menu">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
