<div class="modal-dialog modal-xl" id="get_data_and_fill">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Update Role</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form enctype="multipart/form-data" id="form_update_role">
            <div class="modal-body">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-8">
                                @csrf
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" placeholder="Nhập tên role"
                                           name="role_name" id="role_name" value="{{$role->name}}">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <input type="text" class="form-control" placeholder="Nhập mô tả role"
                                           name="display_name" id="display_name" value="{{$role->display_name}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>
                                    <input type="checkbox" class="checkAll">
                                    check all
                                </label>
                            </div>
                            <div class="col-md-12">
                                @foreach($permissionParent as $item)
                                    <div class="card border-danger mt-3 mb-3 col-md-12 bg-gradient-light"
                                         style="border: 1px solid #a0e80c;">
                                        <div class="card-header">
                                            <label>
                                                <input type="checkbox" value="{{$item->id}}" class="checkbox_wrapper">
                                            </label>
                                            <b>Module {{$item->name}}</b>
                                        </div>
                                        <div class="row">
                                            @foreach($item->permissionChildren as $childItem)
                                                <div class="card-body col-md-3">
                                                    <h5 class="card-title">
                                                        <label><input type="checkbox" value="{{$childItem->id}}"
                                                                      name="permission_id[]"
                                                                      class="checkbox_child" {{$permissionChecked->contains('id',$childItem->id)?'checked':''}}></label>
                                                        {{$childItem->name}}
                                                    </h5>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="btn_update_role" type="button" class="btn btn-primary"
                        data-url="{{route('roles.update',['id'=>$role->id])}}">Update
                </button>
            </div>
        </form>
    </div>
</div>
