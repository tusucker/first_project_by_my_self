<div class="col-md-10" id="table-role" data-url="{{route('roles.table')}}">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
        </thead>
        <tbody id="body_table">
        @if(isset($roles))
            @foreach($roles as $item)
                <tr data-id="{{$item->id}}">
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->display_name}}</td>
                    <td class="text-center w-25">
                        @can('role-edit')
                            <a style="width: 23%" class="show-modal btn btn-info"
                               data-url="{{route('roles.getData',['id'=>$item->id])}}"
                            >Edit</a>
                        @endcan
                        @can('role-delete')
                            <a class="delete-modal btn btn-danger"
                               data-url="{{route('roles.delete',['id'=>$item->id])}}"
                            >Delete</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
