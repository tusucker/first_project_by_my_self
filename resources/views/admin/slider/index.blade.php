@extends('layouts.admin')
@section('title')
    <title>Slider - index</title>
@endsection
@section('css')

@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('partials.content-header',['name'=>'Slider','key'=>'List'])
    <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row padding_left_65">
                    <div class="col-md-12">
                        @can('slider-add')
                            <a class="create-modal btn btn-success m-3 text-bold text-white">Add</a>
                        @endcan
                    </div>
                    <div class="col-md-10" id="table_slider" data-url="{{route('sliders.table')}}">
                    </div>
                    @if(isset($sliders))
                        <div class="col-md-12">{{$sliders->links()}}</div>
                    @endif
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('admin.slider.add')
    @include('admin.slider.edit')
@endsection

@section('js')
    <script src="admins/slider/index/index.js"></script>
    <script src="vendors/sweetAlert/sweetalert2@9.js"></script>
@endsection

