<!-- Extra large modal -->

<div id="modal_update_slider" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Update Slider</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form enctype="multipart/form-data" id="form_update_slider">
                <div class="modal-body">
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    @csrf
                                    <div class="form-group">
                                        <label>Tên Slider</label>
                                        <input type="text" class="form-control" placeholder="Nhập tên slider"
                                               name="name" id="name">
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea rows="4" class="form-control" placeholder="Nhập nội dung slide"
                                                  name="description" id="description"></textarea>
                                    </div>

                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ảnh</label>
                                        <input type="file" class="form-control-file" name="image_path" onchange="readURL(this);">
                                        <div class="row">
                                            <div class="col-12 m-3">
                                                <img src="" alt="" style="width: 200px; height: 200px;object-fit: cover"  id="image_path">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </div>
                    <!-- /.content -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn_update_slider" type="button" class="btn btn-primary" >Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

