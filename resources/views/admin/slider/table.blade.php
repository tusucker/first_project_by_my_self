<div class="col-md-10" id="table_slider" data-url="{{route('sliders.table')}}">
    <table class="table" id="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tên Slider</th>
            <th scope="col">Description</th>
            <th scope="col">Image</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
        </thead>
        <tbody id="body_table">
        @if(isset($sliders))
            @foreach($sliders as $item)
                <tr data-id="{{$item->id}}">
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->description}}</td>
                    <td><img src="{{$item->image_path}}" alt=""
                             style="width:150px;height: 100px;object-fit: cover"></td>
                    <td class="text-center w-25">
                        @can('slider-edit')
                            <a style="width: 23%" class="show-modal btn btn-info"
                               data-id="{{$item->id}}" data-name="{{$item->name}}"
                               data-description="{{$item->description}}"
                               data-image_path="{{$item->image_path}}"
                               data-url_update="{{route('sliders.update',['id'=>$item->id])}}"
                            >Edit</a>
                        @endcan
                        @can('slider-delete')
                            <a class="delete-modal btn btn-danger" id="buton_remove"
                               data-url="{{route('sliders.destroy',['id'=>$item->id])}}"
                            >Delete</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
