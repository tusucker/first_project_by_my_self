<!-- Extra large modal -->

<div id="modal_create_slider" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Create Slider</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form enctype="multipart/form-data" id="form_create_slider">
                <div class="modal-body">
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    @csrf
                                    <div class="form-group">
                                        <label>Tên Slider</label>
                                        <input type="text" class="form-control" placeholder="Nhập tên slider"
                                               name="name">
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea rows="4" class="form-control" placeholder="Nhập nội dung slide"
                                                   name="description"></textarea>
                                    </div>

                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ảnh</label>
                                        <input type="file" class="form-control-file" name="image_path" onchange="readURL(this);">
                                        <img id="slider_image" src="http://placehold.it/200x200" alt="your image" style="object-fit: cover;margin-top: 10px"/>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </div>
                    <!-- /.content -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn_create_slider" type="button" class="btn btn-primary" data-url="{{route('sliders.store')}}">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>

