@extends('layouts.admin')
@section('title')
    <title>Category - index</title>
@endsection
@section('css')

@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('partials.content-header',['name'=>'Category','key'=>'List'])
    <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row padding_left_65">
                    <div class="col-md-12">
                        @can('category-add')
                            <a class="create-modal btn btn-success m-3 text-bold text-white">Add</a>
                        @endcan
                    </div>
                    <div class="col-md-10" id="table_category" data-url="{{route('categories.table')}}">

                    </div>
                    @if(isset($categories))
                        <div class="col-md-12">{{$categories->links()}}</div>
                    @endif
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('admin.category.add')
    @include('admin.category.edit')

@endsection

@section('js')
    <script src="setup/setup.js"></script>
    <script src="category_ajax/index/index.js"></script>
@endsection

