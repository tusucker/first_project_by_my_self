<div class="col-md-10" id="table_category" data-url="{{route('categories.table')}}">
    <table class="table" id="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tên danh mục</th>
            <th scope="col">Tên danh mục cha</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
        </thead>
        <tbody id="body_table">
        @if(isset($categories))
            @foreach($categories as $item)
                <tr data-id="{{$item->id}}" data-parent={{$item->parent_id}}>
                    <td scope="row">{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    @if($item->parent_id==0)
                        <td>Đây là danh mục cha</td>
                    @else
                        <td data-parent_id="{{$item->parent_id}}">{{$item->category['name']}}</td>
                    @endif
                    <td class="text-center w-25">
                        @can('category-edit')
                            <a style="width: 23%" class="show-modal btn btn-info"
                               data-id="{{$item->id}}" data-name="{{$item->name}}"
                               data-parent_id="{{$item->parent_id}}"
                               data-url="{{route('categories.update',['id'=>$item->id])}}"
                            >Edit</a>
                        @endcan
                        @can('category-delete')
                            <a class="delete-modal btn btn-danger" id="buton_remove"
                               data-url="{{route('categories.destroy',['id'=>$item->id])}}"
                            >Delete</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
