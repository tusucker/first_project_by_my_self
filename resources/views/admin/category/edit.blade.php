<div id="update_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalTitle">Sửa danh mục sản phẩm</h4>
            </div>
            <form role="form" id="form_update_category">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Tên danh mục</label>
                        <input type="text" class="form-control"  id="category_name_update" name="name">
                    </div>
                    <div class="form-group">
                        <label>Chọn danh mục cha</label>
                        <select class="form-control" id="category_parent_update" name="parent_id">
                            <option value='0'>Đây là danh mục cha</option>
                            {!! $htmlOption !!}
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning" type="button" id="btn_update_category" >
                            <span class="glyphicon glyphicon-plus"></span>Save
                        </button>
                        <button class="btn btn-warning" type="button" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remobe"></span>Close
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
