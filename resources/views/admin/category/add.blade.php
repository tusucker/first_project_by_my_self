<div id="create" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalTitle">Thêm danh mục sản phẩm</h4>
            </div>
            <form role="form" id="form_add">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Tên danh mục</label>
                        <input type="text" class="form-control" placeholder="Nhập tên danh mục" name="name"
                               id="category_name">
                        <div class="badge badge-danger" id="name_error"
                             style="display: none;font-size: 16px; margin-top: 7px;"></div>
                    </div>
                    <div class="form-group">
                        <label>Chọn danh mục cha</label>
                        <select class="form-control" name="parent_id" id="category_parent">
                            <option value='0'>Đây là danh mục cha</option>
                            {!! $htmlOption !!}
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning" type="button" id="save_category"
                                data-action="{{route('categories.store')}}"
                        >
                            <span class="glyphicon glyphicon-plus"></span>Save
                        </button>
                        <button class="btn btn-warning" type="button" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remobe"></span>Close
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
