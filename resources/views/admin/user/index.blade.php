@extends('layouts.admin')
@section('title')
    <title>User - index</title>
@endsection
@section('css')
    <link href="vendors/select2/select2.min.css" rel="stylesheet"/>
    <link href="admins/user/add/add.css" rel="stylesheet"/>
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('partials.content-header',['name'=>'User','key'=>'List'])
    <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row padding_left_65">
                    @can('user-add')
                        <div class="col-md-12">
                            <a class="create-modal btn btn-success m-3 text-bold text-white">Add</a>
                        </div>
                    @endcan
                    <div class="col-md-10" id="table-user" data-url="{{route('users.table')}}">

                    </div>
                    @if(isset($users))
                        <div class="col-md-12">{{$users->links()}}</div>
                    @endif
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('admin.user.add')
    @include('admin.user.edit')
@endsection

@section('js')
    <script src="vendors/select2/select2.min.js"></script>
    <script>
        $(".select2_init").select2({
            placeholder: "Chọn danh mục",
            allowClear: true
        })
    </script>
    <script src="setup/setup.js"></script>
    <script src="admins/user/index/index.js"></script>
    <script src="vendors/sweetAlert/sweetalert2@9.js"></script>
@endsection

