<div class="col-md-10" id="table-user" data-url="{{route('users.table')}}">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tên User</th>
            <th scope="col">Email</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
        </thead>
        <tbody id="body_table">
        @if(isset($users))
            @foreach($users as $item)
                <tr data-id="{{$item->id}}">
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td class="text-center w-25">
                        @can('user-edit')
                            <a style="width: 23%" class="show-modal btn btn-info"
                               data-url="{{route('users.getData',['id'=>$item->id])}}"
                               data-action="{{route('users.update',['id'=>$item->id])}}"
                            >Edit</a>
                        @endcan
                        @can('user-delete')
                            <a class="delete-modal btn btn-danger"
                               data-url="{{route('users.delete',['id'=>$item->id])}}"
                            >Delete</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
