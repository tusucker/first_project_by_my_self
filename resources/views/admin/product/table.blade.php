<div class="col-md-10" id="table_product" data-url="{{route('products.getList')}}">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tên sản phẩm</th>
            <th scope="col">Giá</th>
            <th scope="col">Hình ảnh</th>
            <th scope="col">Danh mục</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
        </thead>
        <tbody id="body_table_product">
        @if(isset($products))
            @foreach($products as $item)
                <tr data-id_update="{{$item->id}}">
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{number_format($item->price)}}</td>
                    <td><img src="{{$item->avatar}}" alt="" class="img_150_100"></td>
                    <td>{{optional($item->category)->name}}</td>
                    <td class="text-center w-25 ">
                        @can('product-edit')
                            <a id="btn_update_product"
                               class="btn btn-success " style="width: 30%"
                               data-action="{{route('products.getData',['id'=>$item->id])}}"
                               data-url="{{route('products.update',['id'=>$item->id])}}">Edit</a>
                        @endcan
                        @can('product-delete')
                            <a id="btn_delete_product"
                               class="btn btn-danger" style="width: 30%"
                               data-action="{{route('products.delete',['id'=>$item->id])}}">Delete</a>
                        @endcan

                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    {!! $products->appends(Request::only('data_input_search'))->render() !!}
</div>
