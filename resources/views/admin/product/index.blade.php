@extends('layouts.admin')
@section('title')
    <title>Product - index</title>
@endsection
@section('css')
    <link href="admins/product/index/index.css" rel="stylesheet"/>
    <link href="admins/product/add/add.css" rel="stylesheet"/>
    <link href="vendors/select2/select2.min.css" rel="stylesheet"/>
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('partials.content-header',['name'=>'Product','key'=>'List'])
    <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row padding_left_65">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-1">
                                @can('product-add')
                                    <a id="btn_open_modal_create"
                                       class="create-modal btn btn-success text-bold text-white m-3">Add</a>
                                @endcan
                            </div>
                            <div class="col-md-2 m-3">
                                <select class="form-control" id="search_by_category" >
                                    <option value="0">Select Category</option>
                                    {!! $htmlOption !!}}

                                </select>
                            </div>
                            <div class="col-md-2 m-3">
                                <input type="text" class="form-control" id="search_product" data-url="{{route('products.getList')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10" id="table_product" data-url="{{route('products.getList')}}">
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('admin.product.add')
    @include('admin.product.edit')
@endsection
@section('js')
    <script src="vendors/select2/select2.min.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="admins/product/add/add.js"></script>
    <script src="vendors/sweetAlert/sweetalert2@9.js"></script>
    <script src="admins/product/index/index.js"></script>

@endsection
