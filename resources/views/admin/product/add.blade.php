<!-- Extra large modal -->

<div id="modal_create" class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog"
     aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Create Product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form enctype="multipart/form-data" id="form_create_product">
                <div class="modal-body">
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    @csrf
                                    <div class="form-group">
                                        <label>Tên sản phẩm</label>
                                        <input type="text" class="form-control" placeholder="Nhập tên sản phẩm"
                                               name="name" id="product_name">
                                    </div>
                                    <div class="form-group">
                                        <label>Giá sản phẩm</label>
                                        <input type="text" class="form-control" placeholder="Nhập giá sản phẩm"
                                               name="price" id="product_price">
                                    </div>
                                    <div class="form-group">
                                        <label>Ảnh đại diện</label>
                                        <input type="file" class="form-control-file" name="feature_image_path" onchange="readURL(this);">
                                        <img id="product_image" src="http://placehold.it/200x200" alt="your image" style="object-fit: cover;margin-top: 10px"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Ảnh chi tiết</label>
                                        <input type="file" multiple class="form-control-file" name="image_path[]">
                                    </div>

                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Chọn danh mục sản phẩm</label>
                                        <select class="form-control " name="category_id">
                                            {!! $htmlOption !!}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Nhập tags sản phẩm</label>
                                        <select name="tags[]" class="form-control choose_tags" multiple="multiple"
                                                style="width: 100%">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Content</label>
                                        <textarea class="form-control my-editor" rows="6" cols="6"
                                                  name="content" id="getData"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </div>
                    <!-- /.content -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn_create_product" type="button" class="btn btn-primary"
                            data-url="{{route('products.store')}}">Create
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

