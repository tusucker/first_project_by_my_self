@extends('layouts.admin')
@section('title')
    <title>Permission - add</title>
@endsection
@section('css')
    <style>
        .main-footer{
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('partials.content-header',['name'=>'Permission','key'=>'Add'])
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-8">
                        <form role="form" method="post" action="{{route('permissions.store')}}">
                            @csrf
                            <div class="form-group">
                                <label>Chọn phân quyền</label>
                                <select class="form-control" name="module_parent">
                                    <option value="">Chọn module</option>
                                    @foreach(config('permissions.table_module') as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group m-3">
                                <div class="row">
                                    @foreach(config('permissions.module_action') as $item)
                                        <div class="col-3">
                                            <label>
                                                <input type="checkbox" value="{{$item}}" name="module_child[]">
                                                {{$item}}
                                            </label>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('js')

@endsection

