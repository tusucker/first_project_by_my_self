<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['id' => '1', 'name' => 'category', 'display_name' => 'category', 'parent_id' => '0'],
            ['id' => '2', 'name' => 'list', 'display_name' => 'list', 'parent_id' => '1', 'keycode' => config('list-category')],
            ['id' => '3', 'name' => 'add', 'display_name' => 'add', 'parent_id' => '1', 'keycode' => config('add-category')],
            ['id' => '4', 'name' => 'edit', 'display_name' => 'edit', 'parent_id' => '1', 'keycode' => config('edit-category')],
            ['id' => '5', 'name' => 'delete', 'display_name' => 'delete', 'parent_id' => '1', 'keycode' => config('delete-category')],
            ['id' => '6', 'name' => 'menu', 'display_name' => 'menu', 'parent_id' => '0'],
            ['id' => '7', 'name' => 'list', 'display_name' => 'list', 'parent_id' => '6', 'keycode' => config('list-menu')],
            ['id' => '8', 'name' => 'add', 'display_name' => 'add', 'parent_id' => '6', 'keycode' => config('add-menu')],
            ['id' => '9', 'name' => 'edit', 'display_name' => 'edit', 'parent_id' => '6', 'keycode' => config('edit-menu')],
            ['id' => '10', 'name' => 'delete', 'display_name' => 'delete', 'parent_id' => '6', 'keycode' => config('delete-menu')],
            ['id' => '11', 'name' => 'product', 'display_name' => 'product', 'parent_id' => '0'],
            ['id' => '12', 'name' => 'list', 'display_name' => 'list', 'parent_id' => '11', 'keycode' => config('list-product')],
            ['id' => '13', 'name' => 'add', 'display_name' => 'add', 'parent_id' => '11', 'keycode' => config('add-product')],
            ['id' => '14', 'name' => 'edit', 'display_name' => 'edit', 'parent_id' => '11', 'keycode' => config('edit-product')],
            ['id' => '15', 'name' => 'delete', 'display_name' => 'delete', 'parent_id' => '11', 'keycode' => config('delete-product')],
            ['id' => '16', 'name' => 'slider', 'display_name' => 'slider', 'parent_id' => '0'],
            ['id' => '17', 'name' => 'list', 'display_name' => 'list', 'parent_id' => '16', 'keycode' => config('list-slider')],
            ['id' => '18', 'name' => 'add', 'display_name' => 'add', 'parent_id' => '16', 'keycode' => config('add-slider')],
            ['id' => '19', 'name' => 'edit', 'display_name' => 'edit', 'parent_id' => '16', 'keycode' => config('edit-slider')],
            ['id' => '20', 'name' => 'delete', 'display_name' => 'delete', 'parent_id' => '16', 'keycode' => config('delete-slider')],
            ['id' => '21', 'name' => 'user', 'display_name' => 'user', 'parent_id' => '0'],
            ['id' => '22', 'name' => 'list', 'display_name' => 'list', 'parent_id' => '21', 'keycode' => config('list-user')],
            ['id' => '23', 'name' => 'add', 'display_name' => 'add', 'parent_id' => '21', 'keycode' => config('add-user')],
            ['id' => '24', 'name' => 'edit', 'display_name' => 'edit', 'parent_id' => '21', 'keycode' => config('edit-user')],
            ['id' => '25', 'name' => 'delete', 'display_name' => 'delete', 'parent_id' => '21', 'keycode' => config('delete-user')],
            ['id' => '26', 'name' => 'role', 'display_name' => 'role', 'parent_id' => '0'],
            ['id' => '27', 'name' => 'list', 'display_name' => 'list', 'parent_id' => '26', 'keycode' => config('list-role')],
            ['id' => '28', 'name' => 'add', 'display_name' => 'add', 'parent_id' => '26', 'keycode' => config('add-role')],
            ['id' => '29', 'name' => 'edit', 'display_name' => 'edit', 'parent_id' => '26', 'keycode' => config('edit-role')],
            ['id' => '30', 'name' => 'delete', 'display_name' => 'delete', 'parent_id' => '26', 'keycode' => config('delete-role')],
        ]);
    }
}
