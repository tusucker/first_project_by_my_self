<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$fakerVN = \Faker\Factory::create('vi_VN');
$factory->define(App\Product::class, function (Faker $faker) use ($fakerVN) {
    return [
        'name' => $fakerVN->name,
        'price' => $fakerVN->numberBetween(0, 20000),
        'content' => $fakerVN->paragraph(1),
        'category_id' =>factory(App\Category::class)->create()->id,
        'user_id' => factory(App\User::class)->create()->id,
    ];
});
