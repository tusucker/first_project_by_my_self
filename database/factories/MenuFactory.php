<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

//use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Menu::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'parent_id'=>\App\Menu::all()->random()->id,
        'slug'=>\Illuminate\Support\Str::slug('name','-')
    ];
});
