<?php


Route::prefix('categories')->group(function (){
    Route::get('/',[
        'as'=>'categories.index',
        'uses'=>'Admin\CategoryController@index',
        'middleware'=>['can:category-list','check-role']
    ]);
    Route::post('/store',[
        'as'=>'categories.store',
        'uses'=>'Admin\CategoryController@store',
        'middleware'=>'can:category-add'
    ]);
    Route::put('/{id}',[
        'as'=>'categories.update',
        'uses'=>'Admin\CategoryController@update',
        'middleware'=>'can:category-edit'
    ]);
    Route::delete('/{id}',[
        'as'=>'categories.destroy',
        'uses'=>'Admin\CategoryController@destroy',
        'middleware'=>'can:category-delete'
    ]);
    Route::get('/table',[
        'as'=>'categories.table',
        'uses'=>'Admin\CategoryController@table',
    ]);
});
