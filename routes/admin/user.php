<?php


Route::prefix('users')->group(function (){
    Route::get('/',[
        'as'=>'users.index',
        'uses'=>'Admin\UserController@index',
        'middleware'=>'can:user-list'
    ]);
    Route::get('/table',[
        'as'=>'users.table',
        'uses'=>'Admin\UserController@table'
    ]);
    Route::post('/store',[
        'as'=>'users.store',
        'uses'=>'Admin\UserController@store',
        'middleware'=>'can:user-add'
    ]);
    Route::get('/getData/{id}',[
        'as'=>'users.getData',
        'uses'=>'Admin\UserController@getData'
    ]);
    Route::post('/update/{id}',[
        'as'=>'users.update',
        'uses'=>'Admin\UserController@update',
        'middleware'=>'can:user-edit'
    ]);
    Route::get('/delete/{id}',[
        'as'=>'users.delete',
        'uses'=>'Admin\UserController@delete',
        'middleware'=>'can:user-delete'
    ]);
});
