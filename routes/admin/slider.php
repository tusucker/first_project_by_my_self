<?php

Route::prefix('sliders')->group(function (){
    Route::get('/',[
        'as'=>'sliders.index',
        'uses'=>'Admin\SliderController@index',
        'middleware'=>'can:slider-list'
    ]);
    Route::post('/store',[
        'as'=>'sliders.store',
        'uses'=>'Admin\SliderController@store',
        'middleware'=>'can:slider-add'
    ]);
    Route::get('/delete/{id}',[
        'as'=>'sliders.destroy',
        'uses'=>'Admin\SliderController@destroy',
        'middleware'=>'can:slider-delete'
    ]);
    Route::post('/update/{id}',[
        'as'=>'sliders.update',
        'uses'=>'Admin\SliderController@update',
        'middleware'=>'can:slider-edit'
    ]);
    Route::get('/table',[
        'as'=>'sliders.table',
        'uses'=>'Admin\SliderController@table',
    ]);
});
