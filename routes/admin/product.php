<?php


Route::prefix('products')->group(function () {
    Route::get('/', [
        'as' => 'products.index',
        'uses' => 'Admin\ProductController@index',
        'middleware' => 'can:product-list'
    ]);
    Route::post('/store', [
        'as' => 'products.store',
        'uses' => 'Admin\ProductController@store',
        'middleware' => 'can:product-add'
    ]);
    Route::post('/update/{id}', [
        'as' => 'products.update',
        'uses' => 'Admin\ProductController@update',
        'middleware' => 'can:product-edit'
    ]);
    Route::get('/delete/{id}', [
        'as' => 'products.delete',
        'uses' => 'Admin\ProductController@destroy',
        'middleware' => 'can:product-delete'
    ]);
    Route::get('/getData/{id}', [
        'as' => 'products.getData',
        'uses' => 'Admin\ProductController@getData'
    ]);
    Route::get('/getList', [
        'as' => 'products.getList',
        'uses' => 'Admin\ProductController@getList'
    ]);
});
