<?php


Route::prefix('menus')->group(function (){
    Route::get('/',[
        'as'=>'menus.index',
        'uses'=>'Admin\MenuController@index',
        'middleware'=>'can:menu-list'
    ]);
    Route::post('/store',[
        'as'=>'menus.store',
        'uses'=>'Admin\MenuController@store',
        'middleware'=>'can:menu-add'
    ]);
    Route::put('/{id}',[
        'as'=>'menus.update',
        'uses'=>'Admin\MenuController@update',
        'middleware'=>'can:menu-edit'
    ]);
    Route::delete('/{id}',[
        'as'=>'menus.destroy',
        'uses'=>'Admin\MenuController@destroy',
        'middleware'=>'can:menu-delete'
    ]);
    Route::get('/table',[
        'as'=>'menus.table',
        'uses'=>'Admin\MenuController@table',
    ]);
});

