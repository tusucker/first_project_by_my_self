<?php

Route::prefix('/permissions')->group(function(){
        Route::get('/create',[
            'as'=>'permissions.create',
            'uses'=>'Admin\AdminPermissionController@createPermission'
        ]);
        Route::post('/store',[
            'as'=>'permissions.store',
            'uses'=>'Admin\AdminPermissionController@store'
        ]);
    });
