<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = [];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permission', 'role_id', 'permission_id');
    }

    public function getListSortById()
    {
        return $this->orderBy('id', 'desc')->paginate(20);
    }

    public function viewIndex()
    {
        $viewData['roles'] = $this->getListSortById();
        $viewData['permissionParent'] = Permission::where('parent_id', 0)->get();

        return $viewData;
    }


    public function insertRole($request)
    {
        $dataInsert = $request->only(['name', 'display_name']);
        $role = $this->create($dataInsert);
        $role->permissions()->attach($request->permission_id);

        return $role;
    }


    public function updateRole($id, $request)
    {
        $dataUpdate = $request->only(['name', 'display_name']);
        $role = $this->findOrFail($id);
        $role->update($dataUpdate);
        $role->permissions()->sync($request->permission_id);

        return $role;
    }

}
