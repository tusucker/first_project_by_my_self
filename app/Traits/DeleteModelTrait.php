<?php
namespace App\Traits;

use Illuminate\Support\Facades\Log;
use PHPUnit\Exception;

Trait DeleteModelTrait{
    public function delete_modal($model,$id){
        try{
            $model->find($id)->delete();
            return response()->json([
                'code'=>200,
                'message'=>'success'
            ],200);
        }
        catch (Exception $exception){
            Log::error('Message: '.$exception->getMessage());
            return response()->json([
                'message'=>'fail',
                'code'=>500
            ],500);
        }
    }
}
