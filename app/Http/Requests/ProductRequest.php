<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:products,name'.$this->id,
            'price'=>'required|mix:0',
            'content'=>'required'
        ];
    }
    public function messages(){
        return [
            'name.required'=>'the name field is required',
            'name.unique'=>'this name is in used',
            'price.required'=>'the price field is required',
            'price.min'=>'he price must above than 0',
            'content.required'=>'the content field is required'
        ];
    }
}
