<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private $role;
    private $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    public function index()
    {
        $viewData = $this->role->viewIndex();

        return view('admin.role.index', $viewData);
    }

    public function table()
    {
        $roles = $this->role->getListSortById();

        return view('admin.role.table', compact('roles'));
    }

    public function store(Request $request)
    {

        $role = $this->role->insertRole($request);

        return response()->json([
            'data' => $role,
            'message' => 'insert success',
            'code' => '201'
        ], 201);
    }

    public function getData($id)
    {
        $role = $this->role->findOrfail($id);
        $permissionParent = $this->permission->where('parent_id', 0)->get();
        $permissionChecked = $role->permissions;

        return view('admin.role.get_data', compact('role', 'permissionParent', 'permissionChecked'));
    }

    public function update($id, Request $request)
    {
        $role = $this->role->updateRole($id, $request);

        return response()->json([
            'message' => 'update success',
            'code' => 200,
            'data' => $role
        ], 200);
    }

    public function delete($id)
    {
        $this->role->destroy($id);

        return response()->json([
            'message' => 'delete success',
            'code' => 204
        ], 204);
    }
}
