<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function post_login(Request $request)
    {
        $remember = $request->has('remember-me') ? true : false;
        if (auth()->attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ], $remember)) {
            return redirect()->to('home');
        } else
            return redirect()->to('admin/login');
    }
}
