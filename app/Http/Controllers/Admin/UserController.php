<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\Traits\DeleteModelTrait;
use App\Traits\StorageImageTrait;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use DeleteModelTrait;
    use StorageImageTrait;

    private $user;
    private $role;

    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    public function index()
    {
        $roles = $this->role->all();
        $users = $this->user->paginate(10);

        return view('admin.user.index', compact('users', 'roles'));
    }

    public function store(Request $request)
    {
        $user = $this->user->insertUser($request);

        return response()->json([
            'message' => 'success',
            'data' => $user,
            'code' => 201
        ]);
    }

    public function update($id, Request $request)
    {
        $user = $this->user->updateUser($id, $request);

        return response()->json([
            'message' => 'success',
            'data' => $user,
            'code' => 200
        ]);
    }

    public function delete($id)
    {
        $this->user->destroy($id);

        return response()->json([
            'message' => 'delete success',
            'code' => 204
        ], 204);
    }

    public function table()
    {
        $roles = $this->role->all();
        $users = $this->user->paginate(10);

        return view('admin.user.table', compact('users', 'roles'));
    }

    public function getData($id)
    {
        $roles = $this->role->all();
        $user = $this->user->findOrFail($id);
        $roles_user = $user->roles;
        $roleSelected = $this->user->getRoleSelected($roles, $roles_user);

        return response()->json([
            'user' => $user,
            'roleSelected' => $roleSelected
        ], 200);
    }
}
