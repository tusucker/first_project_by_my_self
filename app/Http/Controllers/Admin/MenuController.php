<?php

namespace App\Http\Controllers\Admin;

use App\Components\MenuRecusive;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MenuController extends Controller
{
    private $menuRecusive;
    private $menu;

    public function __construct(MenuRecusive $menuRecusive, Menu $menu)
    {
        $this->menuRecusive = $menuRecusive;
        $this->menu = $menu;
    }

    public function index()
    {
        $viewData = $this->menu->getView();

        return view('admin.menu.index', $viewData);
    }

    public function store(Request $request)
    {
        $menu = $this->menu->insertMenu($request);
        $optionSelect = $this->menuRecusive->menuRecusiveEdit($menu->parent_id);

        return response()->json([
            'data' => $menu,
            'optionSelect' => $optionSelect
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $menu = $this->menu->updateMenu($id, $request);
        $optionSelect = $this->menuRecusive->menuRecusiveEdit($menu->parent_id);

        return response()->json([
            'data' => $menu,
            'optionSelect' => $optionSelect
        ], 200);
    }

    public function destroy($id)
    {
        $this->menu->destroy($id);
        $this->menu->findAndDeleteChild($id);

        return response()->json([
            'message' => 'delete success'
        ], 204);
    }

    public function table(Request $request)
    {
        $menus = $this->menu->search($request->input('search'));
        return view('admin.menu.table', compact('menus'))->render();
    }
}
