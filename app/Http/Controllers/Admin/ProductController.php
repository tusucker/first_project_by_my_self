<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductImage;
use App\ProductTag;
use App\Tag;
use App\Traits\StorageImageTrait;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use StorageImageTrait;

    private $category;
    private $product;
    private $productImage;
    private $tag;
    private $productTag;

    public function __construct(
        Category $category,
        Product $product,
        ProductImage $productImage,
        Tag $tag,
        ProductTag $productTag
    )
    {
        $this->category = $category;
        $this->product = $product;
        $this->productImage = $productImage;
        $this->tag = $tag;
        $this->productTag = $productTag;
    }

    public function index()
    {
        $htmlOption = $this->category->getCategory('');
        //$products = $this->product->getListProduct();

        return view('admin.product.index', compact('htmlOption'));
    }

    public function store(Request $request)
    {
        $product = $this->product->insertProduct($request);
        //insert image detail
        $this->product->insertImageDetail($product, $request);
        //insert tag
        $this->product->insertTag($product, $request->tags);

        $category_name = $product->category->name;

        return response()->json([
            'data' => $product,
            'category_name' => $category_name,
            'code' => 200
        ], 200);
    }

    public function update($id, Request $request)
    {
        //update product
        $product = $this->product->updateProduct($id, $request);
        //update into product_images
        $this->product->updateImageDetail($id, $request, $product);
        //update tag
        $this->product->updateTag($request->tags, $product);
        $category_name = $product->category->name;

        return response()->json([
            'data' => $product,
            'category_name' => $category_name,
            'code' => 200
        ], 200);
    }

    public function destroy($id)
    {
        $this->product->destroy($id);

        return response()->json([
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function getData($id)
    {
        $product = $this->product->with('tempImage')->findOrFail($id);
        $product_image = $product->tempImage()->where('avatar', 0)->get();
        $tags = $product->tags;
        $htmlOption = $this->category->getCategory($product->category_id);

        return response()->json([
            'product' => $product,
            'list_product_image' => $product_image,
            'list_tag' => $tags,
            'htmlOption' => $htmlOption
        ], 200);
    }

    public function getList(Request $request)
    {
        $products = $this->product->getListProduct($request);

        return view('admin.product.table', compact('products'));
    }
}
