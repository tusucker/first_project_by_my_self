<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Slider;
use App\Traits\StorageImageTrait;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    use StorageImageTrait;

    private $slider;

    public function __construct(Slider $slider)
    {
        $this->slider = $slider;
    }
    public function index()
    {
        $sliders = $this->slider->getListSortById();

        return view('admin.slider.index', compact('sliders'));
    }
    public function store(Request $request)
    {
        $slider = $this->slider->insertSlider($request);

        return response()->json([
            'data' => $slider,
            'code' => 200
        ], 200);
    }

    public function update($id, Request $request)
    {
        $slider = $this->slider->updateSlider($request, $id);

        return response()->json([
            'data' => $slider,
            'code' => 200
        ], 200);
    }

    public function destroy($id)
    {
        $this->slider->destroy($id);

        return response()->json([
            'message' => 'delete success',
            'code' => 200
        ], 200);
    }

    public function table()
    {
        $sliders = $this->slider->getListSortById();

        return view('admin.slider.table', compact('sliders'));
    }
}
