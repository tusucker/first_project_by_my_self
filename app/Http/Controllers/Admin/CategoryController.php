<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    private $category;
    private $product;

    public function __construct(Category $category, Product $product)
    {
        $this->category = $category;
        $this->product = $product;
    }

    public function index()
    {
        $viewData = $this->category->getView();

        return view('admin.category.index', $viewData);
    }

    public function store(Request $request)
    {
        $category = $this->category->insertCategory($request);
        $htmlOption = $this->category->getCategory($parentId = '');
        $parent_name = $this->category->getParentName($category);

        return response()->json(
            [
                'data' => $category,
                'htmlOption' => $htmlOption,
                'parent_name' => $parent_name
            ], 201);
    }

    public function update(Request $request, $id)
    {
        $category = $this->category->updateCategory($id, $request);
        $parent_name = $this->category->getParentName($category);
        $htmlOption = $this->category->getCategory($category->parent_id);

        return response()->json(
            [
                'data' => $category,
                'htmlOption' => $htmlOption,
                'parent_name' => $parent_name
            ], 200);
    }

    public function destroy($id)
    {
        $this->category->destroy($id);
        $this->category->findAndDeleteChild($id);
        $htmlOption = $this->category->getCategory($parentId = '');

        return response()->json([
            'message' => 'delete success',
            'htmlOption' => $htmlOption
        ], 204);
    }

    public function table()
    {
        $categories = $this->category->getListSortById();

        return view('admin.category.table', compact('categories'));
    }
}
