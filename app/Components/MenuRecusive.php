<?php
namespace App\Components;
use App\Menu;

class MenuRecusive{
    private $html;
    public function __construct()
    {
        $this->html ='';
    }
    public function menuRecusiveAdd($parentId=0,$subMark =''){
        $data = Menu::all();
        foreach ($data as $item){
            if($item['parent_id'] == $parentId){
                $this->html.="<option value='".$item['id']."'>".$subMark.$item['name']."</option>";
                $this->menuRecusiveAdd($item['id'],$subMark.'--');
            }
        }
        return $this->html;
    }
    public function menuRecusiveEdit($parentId,$id=0,$subMark =''){
        $data = Menu::where('parent_id',$id)->get();
        foreach ($data as $item){
            if(!empty($item['id']) && $item['id'] == $parentId){
                $this->html.="<option selected value='".$item['id']."'>".$subMark.$item['name']."</option>";
            }
            else{
                $this->html.="<option value='".$item['id']."'>".$subMark.$item['name']."</option>";
            }
            $this->menuRecusiveEdit($parentId,$item['id'],$subMark.'--');
        }
        return $this->html;
    }
}
