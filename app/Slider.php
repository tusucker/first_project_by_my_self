<?php

namespace App;

use App\Traits\StorageImageTrait;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use StorageImageTrait;

    protected $guarded = [];

    public function getListSortById()
    {
        return $this->orderBy('id', 'desc')->paginate(20);
    }

    public function insertSlider($request)
    {
        $dataInsert = $request->only(['name', 'description']);
        $dataImageSlider = $this->storageTraitUpload($request, 'image_path', 'slider');
        if (!empty($dataImageSlider)) {
            $dataInsert['image_path'] = $dataImageSlider['file_path'];
            $dataInsert['image_name'] = $dataImageSlider['file_name'];
        }
        $slider = $this->create($dataInsert);

        return $slider;
    }

    public function updateSlider($request, $id)
    {
        $dataUpdate = $request->only(['name', 'description']);
        $dataImageSlider = $this->storageTraitUpload($request, 'image_path', 'slider');
        if (!empty($dataImageSlider)) {
            $dataUpdate['image_path'] = $dataImageSlider['file_path'];
            $dataUpdate['image_name'] = $dataImageSlider['file_name'];
        }
        $slider = $this->findOrFail($id);
        $slider->update($dataUpdate);

        return $slider;
    }
}
