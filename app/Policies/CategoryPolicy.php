<?php

namespace App\Policies;

use App\Category;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;


    public function viewAny(User $user)
    {
        //
    }

    public function view(User $user)
    {
        return $user->checkPermissionAccess(config('permissions.access.list-category'));
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Category $category)
    {
        //
    }

    public function delete(User $user)
    {
        return $user->checkPermissionAccess(config('permissions.access.delete-category'));
    }

    public function add(User $user){
        return $user->checkPermissionAccess(config('permissions.access.add-category'));
    }
    public function edit(User $user){
        return $user->checkPermissionAccess(config('permissions.access.edit-category'));
    }
    public function restore(User $user, Category $category)
    {
        //
    }

    public function forceDelete(User $user, Category $category)
    {
        //
    }
}
