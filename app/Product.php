<?php

namespace App;

use App\Traits\StorageImageTrait;
use Illuminate\Database\Eloquent\Model;

define('path_default', 'http://placehold.it/200x150');

class Product extends Model
{
    use StorageImageTrait;

    protected $guarded = [];

    protected $appends = ['avatar'];

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id');
    }

    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,
            'product_tags',
            'product_id',
            'tag_id'
        )
            ->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function tempImage()
    {
        return $this->morphOne(TempImage::class, 'imageable');
    }

    public function getListProduct($request)
    {
        $data_input_search = $request->input('data_input_search');
        $category_id = $request->input('category_id');

        return $category_id == 0 ?
            $this->when($data_input_search, function ($query) use ($data_input_search) {
                return $query->where('name', 'like', '%' . $data_input_search . '%');
            })
                ->with(['tempImage' => function ($query) {
                    return $query->where('avatar', 1);
                }])->paginate(10)
            :
            $this->when($category_id, function ($query) use ($category_id) {
                return $query->where('category_id', '=', $category_id);
            })
                ->when($data_input_search, function ($query) use ($data_input_search) {
                    return $query->where('name', 'like', '%' . $data_input_search . '%');
                })
                ->with(['tempImage' => function ($query) {
                    return $query->where('avatar', 1);
                }])->paginate(10);
    }

    public function insertProduct($request)
    {
        $dataInsert = $request->only(['name', 'price', 'content', 'category_id']);
        $dataInsert['user_id'] = auth()->id();

        return $product = $this->create($dataInsert);
    }

    public function insertImageDetail($product, $request)
    {
        $dataUploadFeatureImage = $this->storageTraitUpload($request, 'feature_image_path', 'product');
        if (!empty($dataUploadFeatureImage)) {
            $product->tempImage()->create([
                'image_path' => $dataUploadFeatureImage['file_path'],
                'avatar' => 1
            ]);
        }
        foreach ($request->image_path as $item) {
            $dataProductImageDetail = $this->storageTraitUploadMultiple($item, 'product');
            $product->tempImage()->create([
                'image_path' => $dataProductImageDetail['file_path'],
            ]);
        }

        return true;
    }

    public function insertTag($product, $tags)
    {
        foreach ($tags as $item) {
            $tagInstance = Tag::firstOrCreate(['name' => $item]);
            $tagIds[] = $tagInstance->id;
        }
        $product->tags()->attach($tagIds);

        return true;
    }

    public function updateProduct($id, $request)
    {
        $dataUpdate = $request->only(['name', 'price', 'content', 'category_id']);
        $dataUpdate['user_id'] = auth()->id();
        $product = $this->findOrFail($id);
        $product->update($dataUpdate);

        return $product;
    }

    public function updateImageDetail($id, $request, $product)
    {
        $dataUploadFeatureImage = $this->storageTraitUpload($request, 'feature_image_path', 'product');
        if (!empty($dataUploadFeatureImage)) {
            if (count($product->tempImage()->where('avatar', 1)->get()) > 0) {
                $product->tempImage()->where('avatar', 1)->update([
                    'image_path' => $dataUploadFeatureImage['file_path'],
                    'avatar' => 1
                ]);
            } else {
                $product->tempImage()->create([
                    'image_path' => $dataUploadFeatureImage['file_path'],
                    'avatar' => 1
                ]);
            }
        }
        if ($request->hasFile('image_path')) {
            ProductImage::where('product_id', $id)->delete();
            $product->tempImage()->where('avatar', 0)->delete();
            foreach ($request->image_path as $item) {
                $dataProductImageDetail = $this->storageTraitUploadMultiple($item, 'product');
                $product->tempImage()->create([
                    'image_path' => $dataProductImageDetail['file_path'],
                ]);
            }
        }

        return true;
    }

    public function updateTag($tags, $product)
    {
        foreach ($tags as $item) {
            $tagInstance = Tag::firstOrCreate(['name' => $item]);
            $tagIds[] = $tagInstance->id;
        }
        $product->tags()->sync($tagIds);

        return true;
    }

    public function getAvatarAttribute()
    {
        $images = $this->tempImage()->where('avatar', 1)->get();
        if (count($images) > 0) {
            $image_path = $images->first()->image_path;
        } else {
            $image_path = path_default;
        }

        return $image_path;
    }
}
