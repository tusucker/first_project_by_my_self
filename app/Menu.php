<?php

namespace App;

use App\Components\MenuRecusive;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Menu extends Model
{

    protected $fillable = ['name', 'parent_id', 'slug'];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

    public function menuChildren()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }


    public function findAndDeleteChild($id)
    {
        return $this->where('parent_id', $id)->delete();
    }

    public function getView()
    {
        $viewData = [];
        $menuRecusive = new MenuRecusive();
        $viewData['optionSelect'] = $menuRecusive->menuRecusiveAdd();

        return $viewData;
    }

    public function insertMenu($request)
    {
        $dataInsert = $request->all();
        $dataInsert['slug'] = Str::slug($request->name, '-');
        $menu = $this->create($dataInsert);

        return $menu;
    }

    public function updateMenu($id, $request)
    {
        $dataUpdate = $request->all();
        $dataUpdate['slug'] = Str::slug($request->name, '-');
        $menu = $this->findOrFail($id);
        $menu->update($dataUpdate);

        return $menu;
    }

    public function search($input)
    {
        return $this->when($input, function ($query) use ($input) {
            return $query->where('name', 'like', '%' . $input . '%');
        })->paginate(5);
    }

}
