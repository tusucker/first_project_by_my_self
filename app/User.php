<?php

namespace App;

use App\Traits\DeleteModelTrait;
use App\Traits\StorageImageTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;


class User extends Authenticatable
{
    use Notifiable;
    use DeleteModelTrait;
    use StorageImageTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function tempImage()
    {
        return $this->morphOne(TempImage::class, 'imageable');
    }

    public function checkPermissionAccess($permissionCheck)
    {
        $roles = $this->roles;
        foreach ($roles as $role) {
            $permissions = $role->permissions;
            if ($permissions->contains('key_code', $permissionCheck)) {
                return true;
            }
            return false;
        }
    }

    public function checkRoleAccess($roleCheck)
    {
        $rolesUser = $this->roles;

        return $rolesUser->contains('name', $roleCheck);
    }

    public function insertUser($request)
    {
        $dataInsert = $request->only(['name', 'email']);
        $dataInsert['password'] = Hash::make($request->password);
        $user = $this->create($dataInsert);
        $roleIds = $request->user_role;
        $user->roles()->attach($roleIds);

        return $user;
    }

    public function updateUser($id, $request)
    {
        if ($request->password != '') {
            $dataUpdate = $request->only(['name', 'email']);
            $dataUpdate['password'] = Hash::make($request->password);
        } else {
            $dataUpdate = $request->only(['name', 'email']);
        }

        $user = $this->findOrFail($id);
        $user->update($dataUpdate);
        $roleIds = $request->user_role;
        $user->roles()->sync($roleIds);

        return $user;
    }

    public function getRoleSelected($roles, $roles_user)
    {
        $roleSelected = '';
        foreach ($roles as $item) {
            if ($roles_user->contains('id', $item->id)) {
                $roleSelected .= '<option value="' . $item->id . '" selected >' . $item->name . '</option>';
            } else {
                $roleSelected .= '<option value="' . $item->id . '" >' . $item->name . '</option>';
            }
        }

        return $roleSelected;
    }

    public function getFullNameAttribute()
    {
        return $this->name . " dep trai qua";
    }
}
