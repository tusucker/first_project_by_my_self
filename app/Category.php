<?php

namespace App;

use App\Components\Recusive;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{

    protected $fillable = ['name', 'parent_id', 'slug'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function categoryChild()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function product()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function getListSortById()
    {
        return $this->orderBy('id', 'desc')->paginate(20);
    }

    public function getCategory($parentId)
    {
        $data = $this->all();
        $recusive = new Recusive($data);
        $htmlOption = $recusive->categoryRecusive($parentId);

        return $htmlOption;
    }

    public function findAndDeleteChild($id)
    {
        return $this->where('parent_id', $id)->delete();
    }

    public function getParentName($data)
    {
        if ($data->parent_id == 0) {
            $parent_name = 'Đây là danh mục cha';
        } else {
            $parent_name = $data->category['name'];
        }

        return $parent_name;
    }

    public function getView()
    {
        $viewData = [];
        $viewData['categories'] = $this->getListSortById();
        $viewData['htmlOption'] = $this->getCategory($parentId = '');

        return $viewData;
    }

    public function insertCategory($request)
    {
        $dataInsert = $request->all();
        $dataInsert['slug'] = Str::slug($request->name, '-');
        $category = $this->create($dataInsert);

        return $category;
    }

    public function updateCategory($id, $request)
    {
        $dataUpdate = $request->all();
        $dataUpdate['slug'] = Str::slug($request->name, '-');
        $category = $this->findOrFail($id);
        $category->update($dataUpdate);

        return $category;
    }
}
